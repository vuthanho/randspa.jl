module RandSPA

using LinearAlgebra
using Random
using MKL

export spa, randspa, randOrth!

# The succesive projection algorithm (SPA) from paper
# "The successive projections algorithm for variable selection in
# spectroscopic multicomponent analysis", Araujo et al.
# https://doi.org/10.1016/S0169-7439(01)00119-8
function spa(X::AbstractMatrix{T},
             r::Integer,
             epsilon::Float64 = 10e-9) where T <: AbstractFloat
    # Get dimensions
    m, n = size(X)

    # Set of selected indices
    K = zeros(Int, r)

    # Norm of columns of input X
    normX0 = sum.(abs2,eachcol(X))
    # Max of the columns norm
    nXmax = maximum(normX0)
    # Init residual
    normR = copy(normX0)
    U = Matrix{T}(undef,m,r)

    i = 1
    while i <= r && sqrt(maximum(normR)/nXmax) > epsilon
        # Select column of X with largest l2-norm
        # a, b = findmax(normR)
        a = maximum(normR)
        # Check ties up to 1e-6 precision
        b = findall((a .- normR) / a .<= 1e-6)
        # In case of a tie, select column with largest norm of the input matrix
        _, d = findmax(normX0[b])
        b = b[d]
        # Save index of selected column
        K[i] = b
        # Update residual
        U[:,i] .= X[:,b]
        for j in 1:i-1
            U[:,i] .= U[:,i] - U[:,j] * (U[:,j]' * U[:,i])
        end
        U[:,i] ./= norm(U[:,i])
        normR .-= (X'*U[:,i]).^2
        # Increment iterator
        i += 1
    end

    return K
end

# Randomized SPA
function randspa(X::AbstractMatrix{T},
                 r::Integer;
                 d::Integer=1,
                 genP::Function=randn!,
                 Prank::Int=0,
                 returnU::Bool=false) where T <: AbstractFloat
    # Get dimensions
    m, n = size(X)

    # Set of selected indices
    K = zeros(Int, r)
    # Norm of columns of input X
    normX0 = sum.(x->x^2,eachcol(X))

    # Init
    if iszero(Prank) Prank = 2*r end
    P = Matrix{T}(undef,Prank,m)
    U = zeros(T,m,r)
    V = zeros(T,r,n)
    PU = zeros(T,Prank,r)
    PUVj = Vector{T}(undef,Prank)
    PXj = Vector{T}(undef,Prank)
    QnormR = Vector{T}(undef,n)
    dQnormR = Vector{T}(undef,d)
    dK = zeros(Int,d)
    
    for i in 1:r
        for k in 1:d
            genP(P)
            if i>1 mul!(PU,P,U) end
            @views for j in eachindex(QnormR)
                mul!(PXj,P,X[:,j])
                # no need to compute specifically mul!(PUVj,PU[:,1:i-1],V[1:i-1,j]) since PU and V are intialized with zeros
                mul!(PUVj,PU,V[:,j])
                QnormR[j] = sum(abs2,PXj) + sum(abs2,PUVj) - 2*PXj'PUVj 
            end
            # Select column of X with largest l2-norm
            # a, b = findmax(normR)
            a = maximum(QnormR)
            # Check ties up to 1e-6 precision
            b = findall((a .- QnormR) / a .<= 1e-6)
            # In case of a tie, select column with largest norm of the input matrix
            bmax = argmax(normX0[b])
            b = b[bmax]
            # Save index of selected column
            dK[k] = b
            if d>1
                update_UV(U,V,X,i,b)
                dQnormR[k] = sum(abs2,V[i,:])
                U[:,i] .= zero(T)
                V[i,:] .= zero(T)
            end
        end
        b = dK[argmax(dQnormR)]
        K[i] = b
        # Update U and V
        update_UV(U,V,X,i,b)
    end
    if returnU
        return K,U
    else
        return K
    end
end

function update_UV(U,V,X,i,b)
    @views begin
        U[:,i] .= X[:,b]
        for j in 1:i-1
            U[:,i] .-= U[:,j] * (U[:,j]' * U[:,i])
        end
    # U[:,i] ./= norm(U[:,i])
    normalize!(U[:,i])
    mul!(V[i,:],X',U[:,i])
    end
end

# Function similar to Matlab's logspace
logrange(x1, x2, n) = collect(10^y for y in range(log10(x1), log10(x2), length=n))

function randOrth!(P,κ)
    if isinf(κ)
        randn!(P)
        P[2:end,:] .= 0
    elseif κ == 1
        P .= 0
        P[diagind(P)].=1
    else
        randn!(P)
        copy!(P,Matrix(qr(P').Q)[:,1:size(P,1)]')
        rootκ = sqrt(κ)
        # diagP = range(1,rootκ,size(P,1))
        # P .*= diagP
        P[2:end,:] ./= rootκ
        # diagP = logrange(1,1/rootκ,size(P,1))
        # P .*= diagP
    end
end

end # module
