# RandSPA.jl

RandSPA stands for randomized SPA. 
It is a variant of the [successive projection algorithm (SPA)](https://doi.org/10.1016/S0169-7439(01)00119-8).
It is associated with the paper *Randomized Successive Projection Algorithm*, by Olivier Vu Thanh, Nicolas Nadisic, and Nicolas Gillis, published in GRETSI 2022 ([preprint here](https://www.researchgate.net/publication/363055631_Randomized_Successive_Projection_Algorithm)).
If using this code in your research, please cite this paper.


## Problem

RandSPA solves the following problem: 
given a data matrix  $`X \in \mathbb{R}^{m \times n}`$ and a factorization rank $`r \in \mathbb{N}^*`$, find an index set $`\mathcal{J}`$ of cardinality $`r`$ such that $`X \approx X(:,\mathcal{J}) H`$ for some nonnegative matrix $` H \in \mathbb{R}^{r \times n}_+ `$.


## Getting started

* Start Julia
* Type `]` to enter `Pkg` mode
* Add my registry: `registry add https://gitlab.com/vuthanho/vuthanhoregistry`
* Install the RandSPA package: `add RandSPA`
* Leave `Pkg` mode with backspace ⌫

Then you can type `using RandSPA` and use the functions `spa` and `randspa` in your code.


## License

RandSPA.jl is free software, licensed under the [GNU GPL v3](http://www.gnu.org/licenses/gpl.html).
